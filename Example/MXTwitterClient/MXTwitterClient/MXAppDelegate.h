//
//  MXAppDelegate.h
//  MXTwitterClient
//
//  Created by Michael Loistl on 28/06/2013.
//  Copyright (c) 2013 Mobext. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MXViewController;

@interface MXAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) MXViewController *viewController;

@end
