//
//  main.m
//  MXTwitterClient
//
//  Created by Michael Loistl on 28/06/2013.
//  Copyright (c) 2013 Mobext. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MXAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MXAppDelegate class]));
    }
}
