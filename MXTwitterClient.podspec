Pod::Spec.new do |s|

  s.name         =  'MXTwitterClient'
  s.version      =  "0.0.3"
  s.summary      =  'Lightweight and sexy Twitter authentication'
  s.homepage     =  'http://www.mobext.com'
  s.author       =  { 'Dino' => 'dino@mobext.com' }
  s.source       =  { :git => 'https://bitbucket.org/mobext/mxtwitterclient.git', :branch => 'master' }
  s.license      =  'Intern'
  
  # Platform setup
  s.requires_arc = true
  s.ios.deployment_target = '5.0'
  s.osx.deployment_target = '10.7'
  
  s.source_files = 'MXTwitterClient/**/*.{h,m}'
  s.framework  = 'Twitter'
  
end