//
//  MXTwitterClient.h
//  UNAid
//
//  Created by Dino Constantinou on 29/05/2013.
//  Copyright (c) 2013 Mobext. All rights reserved.
//

#import "AFOAuth1Client.h"
#import <Accounts/Accounts.h>

@interface MXTwitterClient : AFOAuth1Client

-(id)initWithKey:(NSString *)key secret:(NSString *)secret url:(NSString *)url;
-(void)signInWithCompletionHandler:(void (^)(NSArray *accounts, NSError *error))completion;
-(void)accessTokenForAccount:(ACAccount *)account completion:(void (^)(AFOAuth1Token *accessToken, NSError *error))completion;
-(void)retrieveTwitterAccountsWithCompletionHandler:(void (^)(NSArray *accounts, NSError *error))completion;
    
@end
