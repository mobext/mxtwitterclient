//
//  MXTwitterClient.m
//  UNAid
//
//  Created by Dino Constantinou on 29/05/2013.
//  Copyright (c) 2013 Mobext. All rights reserved.
//

#import "MXTwitterClient.h"
#import "AFHTTPRequestOperation.h"
#import <Twitter/Twitter.h>

#define MXTwitterClientBaseURL @"https://twitter.com/"

@interface MXTwitterClient ()

@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *key;
@property (nonatomic, strong) NSString *secret;
@property (nonatomic, strong) ACAccountStore *accountStore;
@property (nonatomic, strong) NSDictionary *accessTokens;

-(void)retrieveTwitterAccountsWithCompletionHandler:(void (^)(NSArray *accounts, NSError *error))completion;
-(void)authorizeUsingOAuthWithCompletionHandler:(void (^)(ACAccount *account, NSError *error))completion;
-(void)storeAccountWithAccessToken:(AFOAuth1Token *)token completionHandler:(void (^)(ACAccount *account, NSError *error))completion;
-(void)addAccessToken:(AFOAuth1Token *)accessToken forAccount:(ACAccount *)account;
-(void)acquireReverseOAuthSpecialRequestTokenWithCompletionHandler:(void (^)(NSString *responseString, NSError *error))completionHandler;

@end

@implementation MXTwitterClient

#pragma mark - Property Getters

-(ACAccountStore *)accountStore
{
    if (!_accountStore)
    {
        _accountStore = [[ACAccountStore alloc] init];
    }
    
    return _accountStore;
}

-(NSDictionary *)accessTokens
{
    if (!_accessTokens)
    {
        _accessTokens = [[NSDictionary alloc] init];
    }
    
    return _accessTokens;
}

#pragma mark - Methods

-(id)initWithKey:(NSString *)key secret:(NSString *)secret url:(NSString *)url
{
    self.url = url;
    
    return [super initWithBaseURL:[NSURL URLWithString:MXTwitterClientBaseURL] key:key secret:secret];
}

-(void)signInWithCompletionHandler:(void (^)(NSArray *accounts, NSError *error))completion
{
    [self retrieveTwitterAccountsWithCompletionHandler:^(NSArray *accounts, NSError *error) {

        if (!accounts || [accounts count] == 0)
        {
            [self authorizeUsingOAuthWithCompletionHandler:^(ACAccount *account, NSError *error) {
                
                if (error)
                {
                    completion(nil, error);
                }
                else
                {
                    completion(@[account], nil);
                }

            }];

            return;
        }
        
        completion(accounts, error);
    }];
}

-(void)retrieveTwitterAccountsWithCompletionHandler:(void (^)(NSArray *accounts, NSError *error))completion
{
    ACAccountType *twitterType = [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    
    [self.accountStore requestAccessToAccountsWithType:twitterType options:nil completion:^(BOOL granted, NSError *error) {
        NSArray *accounts = [self.accountStore accountsWithAccountType:twitterType];
        completion(accounts, error);
        
    }];
}

-(void)authorizeUsingOAuthWithCompletionHandler:(void (^)(ACAccount *account, NSError *error))completion
{
    [self authorizeUsingOAuthWithRequestTokenPath:@"oauth/request_token"
                                     userAuthorizationPath:@"oauth/authorize"
                                               callbackURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@success", self.url]]
                                           accessTokenPath:@"oauth/access_token"
                                              accessMethod:@"POST"
                                                     scope:nil success:^(AFOAuth1Token *accessToken, id responseObject) {
                                                         
                                                         [self storeAccountWithAccessToken:accessToken completionHandler:^(ACAccount *account, NSError *error) {
                                                             
                                                             completion(account, nil);
                                                             
                                                         }];
                                                         
                                                     } failure:^(NSError *error) {
                                                         
                                                         completion(nil, error);

                                                     }];
}

-(void)storeAccountWithAccessToken:(AFOAuth1Token *)token completionHandler:(void (^)(ACAccount *account, NSError *error))completion
{
    ACAccountCredential *credential = [[ACAccountCredential alloc] initWithOAuthToken:token.key tokenSecret:token.secret];
    ACAccountType *twitterAcctType = [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    __block ACAccount *newAccount = [[ACAccount alloc] initWithAccountType:twitterAcctType];
    newAccount.credential = credential;

    [self.accountStore saveAccount:newAccount withCompletionHandler:^(BOOL success, NSError *error) {
        if (success)
        {
            completion(newAccount, nil);

            // Save Access Token For Later
            [self addAccessToken:token forAccount:newAccount];
        }
        else
        {
            completion(nil, error);
        }
    }];
}

-(void)accessTokenForAccount:(ACAccount *)account completion:(void (^)(AFOAuth1Token *accessToken, NSError *error))completion
{
    AFOAuth1Token *accessToken = [self.accessTokens objectForKey:account.identifier];
    if (accessToken)
    {
        completion(accessToken, nil);
    }
    else
    {
        [self acquireReverseOAuthSpecialRequestTokenWithCompletionHandler:^(NSString *responseString, NSError *error) {
            
            if (error)
            {
                completion(nil, error);
            }
            else
            {
                NSDictionary *parameters = [[NSMutableDictionary alloc] init];
                [parameters setValue:self.key forKey:@"x_reverse_auth_target"];
                [parameters setValue:responseString forKey:@"x_reverse_auth_parameters"];
                
                TWRequest *request = [[TWRequest alloc] initWithURL:[NSURL URLWithString:@"https://api.twitter.com/oauth/access_token"]
                                                         parameters:parameters
                                                      requestMethod:TWRequestMethodPOST];
                
                [request setAccount:account];
                
                [request performRequestWithHandler: ^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
                    
                    NSString *response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
                    AFOAuth1Token *accessToken = [[AFOAuth1Token alloc] initWithQueryString:response];
                    completion(accessToken, nil);
                    
                }];

            }

        }];
    }
}

-(void)acquireReverseOAuthSpecialRequestTokenWithCompletionHandler:(void (^)(NSString *responseString, NSError *error))completionHandler
{
    // Prepare Parameters
    NSMutableDictionary *parameters = [[self performSelector:@selector(OAuthParameters)] mutableCopy];
    [parameters setObject:@"reverse_auth" forKey:@"x_auth_mode"];
    
    // Make Request
    NSMutableURLRequest *request = [self requestWithMethod:@"POST" path:@"https://api.twitter.com/oauth/request_token/" parameters:parameters];
    [request setHTTPBody:[@"x_auth_mode=reverse_auth" dataUsingEncoding:NSUTF8StringEncoding]];
    
    // Make Operation
    AFHTTPRequestOperation *operation = [self HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
   
        completionHandler(operation.responseString, nil);

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {

        completionHandler(nil, error);
        
    }];
    
    // Enqueue Operation
    [self enqueueHTTPRequestOperation:operation];
}

-(void)addAccessToken:(AFOAuth1Token *)accessToken forAccount:(ACAccount *)account
{
    NSMutableDictionary *accessTokens = [self.accessTokens mutableCopy];
    [accessTokens setObject:accessToken forKey:account.identifier];
    self.accessTokens = [NSDictionary dictionaryWithDictionary:accessTokens];
}

@end
